<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

Auth::routes(['register' => true]);

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function () {
    Route::resource('roles', 'RolesController');
    Route::resource('permissions', 'PermissionsController');
    Route::resource('users', 'UsersController');
    Route::resource('contrats', 'ContratsController');
    Route::resource('personnes', 'PersonnesController');
    Route::resource('pages', 'PagesController');
    Route::resource('activitylogs', 'ActivityLogsController')->only([
        'index', 'show', 'destroy'
    ]);
    Route::resource('admin/settings', 'SettingsController');
    Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
});

Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'AdminController@index');
    Route::get('evaluations/create/{id}', 'EvaluationController@create');
    Route::resource('annee', 'AnneeController');
    Route::resource('cycles', 'CyclesController');
    Route::resource('niveau', 'NiveauController');
    Route::resource('classes', 'ClassesController');
    Route::resource('eleves', 'ElevesController');
    
    Route::resource('matieres', 'MatieresController');
    Route::resource('evaluations', 'EvaluationController');
    Route::resource('notes', 'NoteController');

    //ListeElevesParClasseController

    Route::resource('matieres', 'MatieresController');
});

//Editions
Route::group(['namespace' => 'App\Http\Controllers\Editions', 'prefix' => 'editions', 'middleware' => ['auth']], function () {

    //Liste des Eleves Par Classe
    Route::get('classe/eleves/{classe_id}', 'ListeElevesParClasseController@tousLesEleves')->name('listes.classe.eleves');
    // Exporter les eleves en fichier Excel
    Route::post("eleves/export", "EelveExcelController@export")->name('eleves.excel.export');
});

// Bulletins
Route::get('bulletins/eleves/{eleve}/{eval}', '\App\Http\Controllers\Admin\EleveController@bulletin')->name('bulletins.eleve.eval');
Route::get('bulletins/classe/{classe}/{eval}', '\App\Http\Controllers\Admin\ClassesController@bulletin')->name('bulletins.classe.eval');
Route::resource('bulletins', 'BulletinController');

