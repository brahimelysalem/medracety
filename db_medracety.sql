-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 23, 2021 at 10:50 AM
-- Server version: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_medracety`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `subject_id`, `causer_type`, `causer_id`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'App\\Models\\Annee model has been created', 'App\\Models\\Annee', 1, NULL, NULL, '[]', '2021-02-22 12:44:10', '2021-02-22 12:44:10'),
(2, 'default', 'App\\Models\\Annee model has been updated', 'App\\Models\\Annee', 1, NULL, NULL, '[]', '2021-02-22 12:45:52', '2021-02-22 12:45:52'),
(3, 'default', 'App\\Models\\Annee model has been updated', 'App\\Models\\Annee', 1, NULL, NULL, '[]', '2021-02-22 12:45:59', '2021-02-22 12:45:59'),
(4, 'default', 'App\\Models\\Cycle model has been created', 'App\\Models\\Cycle', 1, NULL, NULL, '[]', '2021-02-22 13:18:30', '2021-02-22 13:18:30'),
(5, 'default', 'App\\Models\\Cycle model has been created', 'App\\Models\\Cycle', 2, NULL, NULL, '[]', '2021-02-22 13:18:47', '2021-02-22 13:18:47'),
(6, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 1, NULL, NULL, '[]', '2021-02-22 13:21:35', '2021-02-22 13:21:35'),
(7, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 2, NULL, NULL, '[]', '2021-02-22 13:23:37', '2021-02-22 13:23:37'),
(8, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 3, NULL, NULL, '[]', '2021-02-22 13:24:30', '2021-02-22 13:24:30'),
(9, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 4, NULL, NULL, '[]', '2021-02-22 13:24:54', '2021-02-22 13:24:54'),
(10, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 5, NULL, NULL, '[]', '2021-02-22 13:26:03', '2021-02-22 13:26:03'),
(11, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 6, NULL, NULL, '[]', '2021-02-22 13:26:36', '2021-02-22 13:26:36'),
(12, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 7, NULL, NULL, '[]', '2021-02-22 13:27:18', '2021-02-22 13:27:18'),
(13, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 1, NULL, NULL, '[]', '2021-02-22 19:08:44', '2021-02-22 19:08:44'),
(14, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 2, NULL, NULL, '[]', '2021-02-22 19:41:18', '2021-02-22 19:41:18'),
(15, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 8, NULL, NULL, '[]', '2021-02-22 19:52:27', '2021-02-22 19:52:27'),
(16, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 9, NULL, NULL, '[]', '2021-02-22 19:52:46', '2021-02-22 19:52:46'),
(17, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 10, NULL, NULL, '[]', '2021-02-22 19:53:16', '2021-02-22 19:53:16'),
(18, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 11, NULL, NULL, '[]', '2021-02-22 19:53:44', '2021-02-22 19:53:44'),
(19, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 12, NULL, NULL, '[]', '2021-02-22 19:54:04', '2021-02-22 19:54:04'),
(20, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 13, NULL, NULL, '[]', '2021-02-22 19:54:24', '2021-02-22 19:54:24'),
(21, 'default', 'App\\Models\\Niveau model has been updated', 'App\\Models\\Niveau', 13, NULL, NULL, '[]', '2021-02-22 19:56:40', '2021-02-22 19:56:40'),
(22, 'default', 'App\\Models\\Niveau model has been updated', 'App\\Models\\Niveau', 12, NULL, NULL, '[]', '2021-02-22 19:57:37', '2021-02-22 19:57:37'),
(23, 'default', 'App\\Models\\Niveau model has been updated', 'App\\Models\\Niveau', 13, NULL, NULL, '[]', '2021-02-22 19:58:01', '2021-02-22 19:58:01'),
(24, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 14, NULL, NULL, '[]', '2021-02-22 19:58:39', '2021-02-22 19:58:39'),
(25, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 15, NULL, NULL, '[]', '2021-02-22 19:58:59', '2021-02-22 19:58:59'),
(26, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 16, NULL, NULL, '[]', '2021-02-22 20:00:10', '2021-02-22 20:00:10'),
(27, 'default', 'App\\Models\\Niveau model has been created', 'App\\Models\\Niveau', 17, NULL, NULL, '[]', '2021-02-22 20:00:36', '2021-02-22 20:00:36'),
(28, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 3, NULL, NULL, '[]', '2021-02-22 21:10:35', '2021-02-22 21:10:35'),
(29, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 4, NULL, NULL, '[]', '2021-02-22 21:10:44', '2021-02-22 21:10:44'),
(30, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 5, NULL, NULL, '[]', '2021-02-22 21:10:55', '2021-02-22 21:10:55'),
(31, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 6, NULL, NULL, '[]', '2021-02-22 21:11:02', '2021-02-22 21:11:02'),
(32, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 7, NULL, NULL, '[]', '2021-02-22 21:11:12', '2021-02-22 21:11:12'),
(33, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 8, NULL, NULL, '[]', '2021-02-22 21:11:19', '2021-02-22 21:11:19'),
(34, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 9, NULL, NULL, '[]', '2021-02-22 21:11:29', '2021-02-22 21:11:29'),
(35, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 10, NULL, NULL, '[]', '2021-02-22 21:11:40', '2021-02-22 21:11:40'),
(36, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 11, NULL, NULL, '[]', '2021-02-22 21:11:49', '2021-02-22 21:11:49'),
(37, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 12, NULL, NULL, '[]', '2021-02-22 21:12:01', '2021-02-22 21:12:01'),
(38, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 13, NULL, NULL, '[]', '2021-02-22 21:12:23', '2021-02-22 21:12:23'),
(39, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 14, NULL, NULL, '[]', '2021-02-22 21:12:36', '2021-02-22 21:12:36'),
(40, 'default', 'App\\Models\\Classe model has been created', 'App\\Models\\Classe', 15, NULL, NULL, '[]', '2021-02-22 21:12:48', '2021-02-22 21:12:48'),
(41, 'default', 'App\\Models\\Matiere model has been created', 'App\\Models\\Matiere', 1, NULL, NULL, '[]', '2021-02-22 23:18:09', '2021-02-22 23:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `annees`
--

CREATE TABLE `annees` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `ouverte` tinyint(1) DEFAULT NULL,
  `precedente_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `annees`
--

INSERT INTO `annees` (`id`, `created_at`, `updated_at`, `deleted_at`, `libelle`, `date_debut`, `date_fin`, `ouverte`, `precedente_id`) VALUES
(1, '2021-02-22 12:44:10', '2021-02-22 12:45:59', NULL, '2020 - 2021', '2020-11-16', '2021-07-30', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `niveau_id` bigint(20) DEFAULT NULL,
  `annee_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `created_at`, `updated_at`, `deleted_at`, `numero`, `niveau_id`, `annee_id`) VALUES
(1, '2021-02-22 19:08:44', '2021-02-22 19:08:44', NULL, 1, 1, 1),
(2, '2021-02-22 19:41:18', '2021-02-22 19:41:18', NULL, 1, 2, 1),
(3, '2021-02-22 21:10:35', '2021-02-22 21:10:35', NULL, 1, 3, 1),
(4, '2021-02-22 21:10:44', '2021-02-22 21:10:44', NULL, 1, 4, 1),
(5, '2021-02-22 21:10:55', '2021-02-22 21:10:55', NULL, 1, 5, 1),
(6, '2021-02-22 21:11:02', '2021-02-22 21:11:02', NULL, 1, 6, 1),
(7, '2021-02-22 21:11:12', '2021-02-22 21:11:12', NULL, 1, 7, 1),
(8, '2021-02-22 21:11:19', '2021-02-22 21:11:19', NULL, 1, 8, 1),
(9, '2021-02-22 21:11:29', '2021-02-22 21:11:29', NULL, 1, 9, 1),
(10, '2021-02-22 21:11:40', '2021-02-22 21:11:40', NULL, 1, 10, 1),
(11, '2021-02-22 21:11:49', '2021-02-22 21:11:49', NULL, 1, 11, 1),
(12, '2021-02-22 21:12:01', '2021-02-22 21:12:01', NULL, 1, 13, 1),
(13, '2021-02-22 21:12:23', '2021-02-22 21:12:23', NULL, 1, 12, 1),
(14, '2021-02-22 21:12:36', '2021-02-22 21:12:36', NULL, 1, 14, 1),
(15, '2021-02-22 21:12:48', '2021-02-22 21:12:48', NULL, 1, 16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cycles`
--

CREATE TABLE `cycles` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cycles`
--

INSERT INTO `cycles` (`id`, `created_at`, `updated_at`, `libelle`) VALUES
(1, '2021-02-22 13:18:30', '2021-02-22 13:18:30', 'Primaire'),
(2, '2021-02-22 13:18:47', '2021-02-22 13:18:47', 'Secondaire');

-- --------------------------------------------------------

--
-- Table structure for table `eleves`
--

CREATE TABLE `eleves` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nni` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `lieu_naissance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lieu_naissance_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classe_id` bigint(20) DEFAULT NULL,
  `date_inscription` date DEFAULT NULL,
  `mensualite` double(8,2) DEFAULT NULL,
  `frais_inscription` double(8,2) DEFAULT NULL,
  `exonere` tinyint(1) DEFAULT NULL,
  `date_sortie` date DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `matricule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `moyenne_generale` double(8,2) DEFAULT NULL,
  `decision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `matieres`
--

CREATE TABLE `matieres` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coeficient` int(11) DEFAULT NULL,
  `base` int(11) DEFAULT NULL,
  `niveau_id` bigint(20) DEFAULT NULL,
  `annee_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `matieres`
--

INSERT INTO `matieres` (`id`, `created_at`, `updated_at`, `deleted_at`, `libelle`, `coeficient`, `base`, `niveau_id`, `annee_id`) VALUES
(1, '2021-02-22 23:18:09', '2021-02-22 23:18:09', NULL, 'Lecture', 1, 10, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_193651_create_roles_permissions_tables', 1),
(4, '2018_08_01_183154_create_pages_table', 1),
(5, '2018_08_04_122319_create_settings_table', 1),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(7, '2021_02_22_111447_create_activity_log_table', 1),
(8, '2021_02_22_112533_create_annees_table', 2),
(9, '2021_02_22_131201_create_cycles_table', 3),
(10, '2021_02_22_131426_create_niveaux_table', 4),
(11, '2021_02_22_190120_create_classes_table', 5),
(12, '2021_02_22_214244_create_eleves_table', 6),
(13, '2021_02_22_221309_create_matieres_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `niveaux`
--

CREATE TABLE `niveaux` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `libelle_fr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `libelle_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `niveaux`
--

INSERT INTO `niveaux` (`id`, `created_at`, `updated_at`, `libelle`, `libelle_fr`, `libelle_ar`) VALUES
(1, '2021-02-22 13:21:35', '2021-02-22 13:21:35', 'Crèche', 'Crèche', 'الروضة'),
(2, '2021-02-22 13:23:37', '2021-02-22 13:23:37', '1AF', 'Prémière année fondamentale', 'السنة الأولى إبتدائية'),
(3, '2021-02-22 13:24:30', '2021-02-22 13:24:30', '2AF', 'Deuxième année fondamentale', NULL),
(4, '2021-02-22 13:24:54', '2021-02-22 13:24:54', '3AF', 'Troisième année fondamentale', NULL),
(5, '2021-02-22 13:26:03', '2021-02-22 13:26:03', '4AF', 'Quatrième année fondamentale', NULL),
(6, '2021-02-22 13:26:36', '2021-02-22 13:26:36', '5AF', 'Cinquième année fondamentale', NULL),
(7, '2021-02-22 13:27:18', '2021-02-22 13:27:18', '6AF', 'Sixième année fondamentale', NULL),
(8, '2021-02-22 19:52:27', '2021-02-22 19:52:27', '1AS', 'Prémière année secondaire', NULL),
(9, '2021-02-22 19:52:46', '2021-02-22 19:52:46', '2AS', 'Deuxième année secondaire', NULL),
(10, '2021-02-22 19:53:16', '2021-02-22 19:53:16', '3AS', 'Troisième année secondaire', NULL),
(11, '2021-02-22 19:53:44', '2021-02-22 19:53:44', '4AS', 'Quatrième année secondaire', NULL),
(12, '2021-02-22 19:54:04', '2021-02-22 19:57:37', '5AS-C', 'Cinquième année série Mathématique', NULL),
(13, '2021-02-22 19:54:24', '2021-02-22 19:58:01', '5AS-D', 'Cinquième année série SN', NULL),
(14, '2021-02-22 19:58:39', '2021-02-22 19:58:39', '6AS-D', 'Sixième année serie SN', NULL),
(15, '2021-02-22 19:58:59', '2021-02-22 19:58:59', '6AS-C', 'Sixième année série Mathématique', NULL),
(16, '2021-02-22 20:00:10', '2021-02-22 20:00:10', '7AS-D', 'Septième année série SN', NULL),
(17, '2021-02-22 20:00:36', '2021-02-22 20:00:36', '7AS-C', 'Septième année série Mathématique', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subject` (`subject_type`,`subject_id`),
  ADD KEY `causer` (`causer_type`,`causer_id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `annees`
--
ALTER TABLE `annees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cycles`
--
ALTER TABLE `cycles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eleves`
--
ALTER TABLE `eleves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `matieres`
--
ALTER TABLE `matieres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `niveaux`
--
ALTER TABLE `niveaux`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `annees`
--
ALTER TABLE `annees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `cycles`
--
ALTER TABLE `cycles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `eleves`
--
ALTER TABLE `eleves`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `matieres`
--
ALTER TABLE `matieres`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `niveaux`
--
ALTER TABLE `niveaux`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
