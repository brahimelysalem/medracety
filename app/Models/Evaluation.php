<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evaluation extends Model
{
    use SoftDeletes;

    public $table = 'evaluations';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
        'date_evaluation',
    ];

    protected $fillable = [
        'type',
        'matiere_id',
        'classe_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_evaluation',
    ];

    const TYPE_SELECT = [
        'D'  => 'إختبار',
        'C1' => 'إمتحان الفصل الأول',
        'C2' => 'إمتحان الفصل الثاني',
        'C3' => 'إمتحان التجاوز',
    ];

    public function evaluationNotes()
    {
        return $this->hasMany(Note::class, 'evaluation_id', 'id');
    }

    public function getDateEvaluationAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateEvaluationAttribute($value)
    {
        $this->attributes['date_evaluation'] = $value ? Carbon::createFromFormat('Y-m-d', $value)->format('Y-m-d') : null;
    }

    public function matiere()
    {
        return $this->belongsTo(Matiere::class, 'matiere_id');
    }
    public function classe()
    {
        return $this->belongsTo(Classe::class, 'classe_id');
    }
}
