<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Classe extends Model
{
    use LogsActivity;
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'classes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    const ETAT_RADIO = [
        '0'  => 'Fermée',
        '1' => 'Ouverte',
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['numero', 'niveau_id', 'annee_id'];

    public function niveau()
    {
        return $this->belongsTo('App\Models\Niveau');
    }
    public function eleves()
    {
        return $this->hasMany(Eleve::class);
    }

    public function annee()
    {
        return $this->belongsTo('App\Models\Annee');
    }


    public function matieres()
    {
        return Matiere::where('niveau_id',$this->niveau_id)->where('annee_id',$this->annee_id)->get();
    }
    
    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
