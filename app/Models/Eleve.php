<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Eleve extends Model
{
    use LogsActivity;
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'eleves';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $casts = [
        'numero' => 'integer',
    ];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nni', 'prenom', 'prenom_ar', 'nom', 'nom_ar', 'date_naissance', 'lieu_naissance', 'lieu_naissance_ar', 'classe_id', 'date_inscription', 'mensualite', 'frais_inscription', 'exonere', 'date_sortie', 'telephone', 'telephone_2', 'matricule', 'moyenne_generale', 'decision', 'numero'];

    public function classe()
    {
        return $this->belongsTo('App\Models\Classe');
    }
    public function niveau()
    {
        return $this->classe->niveau;
    }
    public function setMG($mg)
    {
        $this->mg = $mg;
        $this->save();
    }

    public function notes()
    {
        return $this->hasMany('App\Models\Note');
    }


    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
