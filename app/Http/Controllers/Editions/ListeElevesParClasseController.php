<?php

namespace App\Http\Controllers\Editions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use \App\Models\Classe;
use PDF;

class ListeElevesParClasseController extends Controller
{
    //
    /**
     * Rediriger ves la liste des classes si aucune n'est specifiée.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return  redirect("classes.index");
    }

    /**
     * Rediriger ves la liste des classes si aucune n'est specifiée.
     *
     * @return \Illuminate\View\View
     */
    public function tousLesEleves_dom($classe_id)
    {
        $classe = Classe::findOrFail($classe_id);

        $liste = PDF::loadView('editions.classe.liste', ['classe'=>$classe]);

        //return $liste->download('Liste'.'-'.$classe->niveau->libelle.'-'.$classe->annee->libelle.'.pdf');
        //return $liste->stream('Liste'.'-'.$classe->niveau->libelle.'-'.$classe->annee->libelle.'.pdf');
        return $liste->stream();
        return  view("classes.index" , compact([$classe]));
    }

    public function tousLesEleves($classe_id){

        $classe = Classe::findOrFail($classe_id);

        $view = \View::make('editions.classe.liste',compact('classe'));
        $html = $view->render();

        PDF::SetTitle('Liste'.'-'.$classe->niveau->libelle.'-'.$classe->annee->libelle);
        //PDF::setRTL(true);
        // set font
        PDF::SetFont('dejavusans', '', 12, '', true);
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('Liste'.'-'.$classe->niveau->libelle.'-'.$classe->annee->libelle.'.pdf');
    }
}
