<?php

namespace App\Http\Controllers\Editions;
use App\Http\Controllers\Controller;
use Spatie\SimpleExcel\SimpleExcelWriter;
use Spatie\SimpleExcel\SimpleExcelReader;

use Illuminate\Http\Request;

use \App\Models\Eleve;
use \App\Models\Classe;
use \App\Models\Niveau;

class EelveExcelController extends Controller
{
    //

    // Exporter les données
    public function export (Request $request) {

    	// 1. Validation des informations du formulaire
    	$this->validate($request, [ 
    		'name' => 'bail|required|string',
    		'extension' => 'bail|required|string|in:xlsx,csv'
    	]);

    	// 2. Le nom du fichier avec l'extension : .xlsx ou .csv
    	$file_name = $request->name.".".$request->extension;

    	// 3. On récupère données de la table "eleves"
    	$eleves = Eleve::join("classes",'classe_id',"=",'classes.id')
		->join("niveaux",'niveau_id',"=",'niveaux.id')
		->select("eleves.numero", "prenom_ar","nom_ar","prenom","nom", "nni","date_naissance", "lieu_naissance_ar" ,"matricule","niveaux.libelle","genre" )
		->orderby("niveau_id",'asc' )
		->orderByRaw("eleves.numero",'asc' )->get();

    	// 4. $writer : Objet Spatie\SimpleExcel\SimpleExcelWriter
    	$writer = SimpleExcelWriter::streamDownload($file_name);

 		// 5. On insère toutes les lignes au fichier Excel $file_name
    	$writer->addRows($eleves->toArray());

        // 6. Lancer le téléchargement du fichier
        $writer->toBrowser();

    }
}
