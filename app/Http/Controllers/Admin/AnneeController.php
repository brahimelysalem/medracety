<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Annee;
use Illuminate\Http\Request;

class AnneeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $annee = Annee::where('libelle', 'LIKE', "%$keyword%")
                ->orWhere('date_debut', 'LIKE', "%$keyword%")
                ->orWhere('date_fin', 'LIKE', "%$keyword%")
                ->orWhere('ouverte', 'LIKE', "%$keyword%")
                ->orWhere('precedente_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $annee = Annee::latest()->paginate($perPage);
        }

        return view('annee.annee.index', compact('annee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('annee.annee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'libelle' => 'required',
			'date_debut' => 'required',
			'date_fin' => 'required',
			'ouverte' => 'required'
		]);
        $requestData = $request->all();

        Annee::create($requestData);

        return redirect('annee/annee')->with('flash_message', 'Annee added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $annee = Annee::findOrFail($id);

        return view('annee.annee.show', compact('annee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $annee = Annee::findOrFail($id);

        return view('annee.annee.edit', compact('annee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'libelle' => 'required',
			'date_debut' => 'required',
			'date_fin' => 'required',
			'ouverte' => 'required'
		]);
        $requestData = $request->all();

        $annee = Annee::findOrFail($id);
        $annee->update($requestData);

        return redirect('annee/annee')->with('flash_message', 'Annee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Annee::destroy($id);

        return redirect('annee/annee')->with('flash_message', 'Annee deleted!');
    }
}
