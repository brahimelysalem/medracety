<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EvaluationNotesController extends Controller
{
    public function index()
    {
        //abort_if(Gate::denies('evaluation_note_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.evaluationNotes.index');
    }
}
