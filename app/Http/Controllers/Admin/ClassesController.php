<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Annee;
use App\Models\Niveau;
use App\Models\Classe;
use Illuminate\Http\Request;

use PDF;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $classes = Classe::where('numero', 'LIKE', "%$keyword%")
                ->orWhere('niveau_id', 'LIKE', "%$keyword%")
                ->orWhere('annee_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $classes = Classe::latest()->paginate($perPage);
        }

        return view('admin.classes.index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $niveaux = Niveau::all();
        $annees = Annee::all();
        return view('admin.classes.create', compact('niveaux','annees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'numero' => 'required',
			'niveau_id' => 'required',
			'annee_id' => 'required'
		]);
        $requestData = $request->all();

        Classe::create($requestData);

        return redirect('admin/classes')->with('flash_message', 'Class added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $class = Classe::findOrFail($id);

        return view('admin.classes.show', compact('class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $class = Classe::findOrFail($id);
        $niveaux = Niveau::all();
        $annees = Annee::all();

        return view('admin.classes.edit', compact('class','niveaux','annees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'numero' => 'required',
			'niveau_id' => 'required',
			'annee_id' => 'required'
		]);
        $requestData = $request->all();

        $class = Classe::findOrFail($id);
        $class->update($requestData);

        return redirect('admin/classes')->with('flash_message', 'Class updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Classe::destroy($id);

        return redirect('admin/classes')->with('flash_message', 'Class deleted!');
    }
    
    public function bulletin(int $classe_id,string $type_evaluation)
    {
        $classe = Classe::find($classe_id);
        return view('admin.classes.bulletins', compact('classe' ,'type_evaluation'));
        if($type_evaluation=='C1'){
            //$pdf = \PDF::loadView('admin.classes.bulletins',compact('classe'));
            $view = \View::make('admin.classes.bulletins',compact('classe','type_evaluation'));
            $html = $view->render();
    
            PDF::SetTitle('Liste'.'-'.$classe->niveau->libelle.'-'.$classe->annee->libelle);
            //PDF::setRTL(true);
            // set font
            PDF::SetFont('dejavusans', '', 12, '', true);
            PDF::AddPage();
            PDF::writeHTML($html, true, false, true, false, '');
            PDF::Output('Liste'.'-'.$classe->niveau->libelle.'-'.$classe->annee->libelle.'.pdf');
        }
        if($type_evaluation=='C2'){
           // $pdf = \PDF::loadView('admin.classes.bulletins2',compact('classe'));
        }
        if($type_evaluation=='C3'){
           // $pdf = \PDF::loadView('admin.classes.bulletins3',compact('classe'));
        }
            $name = "classe-".$classe->id.".pdf";
            //$pdf->setPaper('A4', 'landscape');
            return $pdf->stream('test_pdf.pdf');
            //$pdf->save(storage_path('b.pdf'));

        //return 'OK';

        return view('admin.classes.bulletin', compact('classe'));
    }

}
