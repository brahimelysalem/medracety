<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Cycle;
use Illuminate\Http\Request;

class CyclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cycles = Cycle::where('libelle', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $cycles = Cycle::latest()->paginate($perPage);
        }

        return view('admin.cycles.index', compact('cycles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.cycles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'libelle' => 'required'
		]);
        $requestData = $request->all();
        
        Cycle::create($requestData);

        return redirect('admin/cycles')->with('flash_message', 'Cycle added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cycle = Cycle::findOrFail($id);

        return view('admin.cycles.show', compact('cycle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cycle = Cycle::findOrFail($id);

        return view('admin.cycles.edit', compact('cycle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'libelle' => 'required'
		]);
        $requestData = $request->all();
        
        $cycle = Cycle::findOrFail($id);
        $cycle->update($requestData);

        return redirect('admin/cycles')->with('flash_message', 'Cycle updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Cycle::destroy($id);

        return redirect('admin/cycles')->with('flash_message', 'Cycle deleted!');
    }
}
