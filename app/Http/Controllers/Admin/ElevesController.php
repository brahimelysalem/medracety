<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Classe;
use App\Models\Eleve;
use Illuminate\Http\Request;

class ElevesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $eleves = Eleve::where('nni', 'LIKE', "%$keyword%")
                ->orWhere('prenom', 'LIKE', "%$keyword%")
                ->orWhere('prenom_ar', 'LIKE', "%$keyword%")
                ->orWhere('nom', 'LIKE', "%$keyword%")
                ->orWhere('nom_ar', 'LIKE', "%$keyword%")
                ->orWhere('date_naissance', 'LIKE', "%$keyword%")
                ->orWhere('lieu_naissance', 'LIKE', "%$keyword%")
                ->orWhere('lieu_naissance_ar', 'LIKE', "%$keyword%")
                ->orWhere('classe_id', 'LIKE', "%$keyword%")
                ->orWhere('date_inscription', 'LIKE', "%$keyword%")
                ->orWhere('mensualite', 'LIKE', "%$keyword%")
                ->orWhere('frais_inscription', 'LIKE', "%$keyword%")
                ->orWhere('exonere', 'LIKE', "%$keyword%")
                ->orWhere('date_sortie', 'LIKE', "%$keyword%")
                ->orWhere('telephone', 'LIKE', "%$keyword%")
                ->orWhere('telephone_2', 'LIKE', "%$keyword%")
                ->orWhere('matricule', 'LIKE', "%$keyword%")
                ->orWhere('moyenne_generale', 'LIKE', "%$keyword%")
                ->orWhere('decision', 'LIKE', "%$keyword%")
                ->orWhere('numero', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $eleves = Eleve::latest()->paginate($perPage);
        }

        return view('admin.eleves.index', compact('eleves'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $classes = Classe::all();
        return view('admin.eleves.create',compact('classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'prenom' => 'required',
			'nom' => 'required',
			'classe_id' => 'required',
			'mensualite' => 'required',
			'telephone' => 'required'
		]);
        $requestData = $request->all();

        Eleve::create($requestData);

        return redirect('admin/eleves')->with('flash_message', 'Eleve added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $Eleve = Eleve::findOrFail($id);

        return view('admin.eleves.show', compact('Eleve'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $Eleve = Eleve::findOrFail($id);
        $classes = Classe::all();

        return view('admin.eleves.edit', compact('Eleve','classes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'prenom' => 'required',
			'nom' => 'required',
			'classe_id' => 'required',
			'mensualite' => 'required',
			'telephone' => 'required'
		]);
        $requestData = $request->all();

        $Eleve = Eleve::findOrFail($id);
        $Eleve->update($requestData);

        return redirect('admin/eleves')->with('flash_message', 'Eleve updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Eleve::destroy($id);

        return redirect('admin/eleves')->with('flash_message', 'Eleve deleted!');
    }
    public function bulletin(int $eleve_id,string $type_evaluation)
    {
        $eleve = eleve::find($eleve_id);

        if($type_evaluation=='C1'){
            $pdf = \PDF::loadView('admin.eleves.bulletin',compact('eleve'));
        }
        if($type_evaluation=='C2'){
            $pdf = \PDF::loadView('admin.eleves.bulletin2',compact('eleve'));
        }
        if($type_evaluation=='C3'){
            $pdf = \PDF::loadView('admin.eleves.bulletin3',compact('eleve'));
        }
            $name = "sudent-".$eleve->id.".pdf";
            //$pdf->setPaper('A4', 'landscape');
            //return $pdf->stream('test_pdf.pdf');
            //$pdf->save(storage_path('b.pdf'));

        //return 'OK';

        return view('admin.eleves.bulletin', compact('eleve'));
    }
}
