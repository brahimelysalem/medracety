<?php

namespace App\Http\Controllers\Admin;

use App\Models\Evaluation;
use App\Http\Controllers\Controller;

use App\Models\Matiere;
use App\Models\Classe;
use App\Models\Note;
use App\Models\Eleve;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EvaluationController extends Controller
{
    public function index(Request $request)
    {
        //abort_if(Gate::denies('evaluation_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $evaluations = Evaluation::all();
        $classes = Classe::all();
        $classe_id = "";
        $type = "";

        if($request->classe_id){
            $evaluations = $evaluations->where('classe_id' , $request->classe_id);
            $classe_id = $request->classe_id;
        }

        if($request->type){
            $evaluations = $evaluations->where('type' , $request->type);
            $type = $request->type;
        }

        return view('admin.evaluations.index', compact('evaluations', 'classes', 'classe_id', 'type'));
    }

    public function create($id = null)
    {
        //abort_if(Gate::denies('evaluation_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if($id==null){
            $classes = Classe::all();//->pluck('nom', 'id')->prepend(trans('Slectionner ...'), '');
            //dd($classes);
            return view('admin.evaluations.classes', compact('classes'));
        }
        else{
            $classe = Classe::find($id);
            $matieres = Matiere::where('niveau_id',$classe->niveau_id)->get()->pluck('libelle', 'id')->prepend(trans('Slectionner ...'), '');
            //dd($matieres);
            return view('admin.evaluations.create', compact('matieres','classe'));
        }
    }

    public function store(Request $request)
    {
        $evaluation = Evaluation::create($request->all());
        //dd($evaluation);
        $eleves = Eleve::where('classe_id',$evaluation->classe_id)->get();

        foreach ($eleves as $key => $eleve) {
            $note = Note::create([ 'eleve_id' => $eleve->id  , 'evaluation_id' => $evaluation->id , 'valeur' => 0 ]);
        }

        return redirect()->route('evaluations.index');
    }

    public function edit(Evaluation $evaluation)
    {
        //abort_if(Gate::denies('evaluation_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        //$matieres = Matiere::all()->pluck('libelle', 'id')->prepend(trans('Slectionner ...'), '');
        $classe = Classe::find($evaluation->classe_id);
        $matieres = Matiere::where('niveau_id',$classe->niveau_id)->get()->pluck('libelle', 'id')->prepend(trans('Slectionner ...'), '');

        $evaluation->load('matiere');

        return view('admin.evaluations.edit', compact('matieres', 'evaluation'));
    }

    public function update(Request $request, Evaluation $evaluation)
    {
        $evaluation->update($request->all());
        foreach ($request->notes as $key => $value) {

            $note = Note::find($key);
            $note->valeur = $value;
            $note->save();
        }

        return redirect()->route('evaluations.index');
    }

    public function show(Evaluation $evaluation)
    {
        //abort_if(Gate::denies('evaluation_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $evaluation->load('matiere');

        return view('admin.evaluations.show', compact('evaluation'));
    }

    public function destroy(Evaluation $evaluation)
    {
        //abort_if(Gate::denies('evaluation_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        foreach ($evaluation->evaluationNotes as $key => $note) {
            $note->delete();
        }
        $evaluation->delete();

        return back();
    }

    public function massDestroy(Request $request)
    {
        Evaluation::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
