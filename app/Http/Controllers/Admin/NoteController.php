<?php

namespace App\Http\Controllers\Admin;

use App\Models\Evaluation;
use App\Http\Controllers\Controller;

use App\Models\Eleve;
use App\Models\Note;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NoteController extends Controller
{
    public function index()
    {
        //abort_if(Gate::denies('note_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $notes = Note::all();

        return view('admin.notes.index', compact('notes'));
    }

    public function create()
    {
        //abort_if(Gate::denies('note_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        //$eleves = Eleve::all()->pluck('nom', 'id')->prepend(trans('Slectionner ...'), '');
        $eleves = Eleve::orderBy('classe_id')->get();

        //$evaluations = Evaluation::all()->pluck('date_evaluation', 'id')->prepend(trans('Slectionner ...'), '');
        $evaluations = Evaluation::orderBy('classe_id')->get();
        return view('admin.notes.create', compact('eleves', 'evaluations'));
    }

    public function store(Request $request)
    {
        $note = Note::create($request->all());

        return redirect()->route('notes.index');
    }

    public function edit(Note $note)
    {
        //abort_if(Gate::denies('note_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $eleves = Eleve::all()->pluck('nom', 'id')->prepend(trans('Slectionner ...'), '');

        $evaluations = Evaluation::all()->pluck('date_evaluation', 'id')->prepend(trans('Slectionner ...'), '');

        $note->load('eleve', 'evaluation');

        return view('admin.notes.edit', compact('eleves', 'evaluations', 'note'));
    }

    public function update(Request $request, Note $note)
    {
        $note->update($request->all());

        return redirect()->route('notes.index');
    }

    public function show(Note $note)
    {
        //abort_if(Gate::denies('note_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $note->load('eleve', 'evaluation');

        return view('admin.notes.show', compact('note'));
    }

    public function destroy(Note $note)
    {
        //abort_if(Gate::denies('note_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $note->delete();

        return back();
    }

    public function massDestroy(Request $request)
    {
        Note::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
