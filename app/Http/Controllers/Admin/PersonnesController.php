<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Personne;
use Illuminate\Http\Request;

class PersonnesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $personnes = Personne::where('telephone', 'LIKE', "%$keyword%")
                ->orWhere('telephone_2', 'LIKE', "%$keyword%")
                ->orWhere('nom', 'LIKE', "%$keyword%")
                ->orWhere('prenom', 'LIKE', "%$keyword%")
                ->orWhere('categorie', 'LIKE', "%$keyword%")
                ->orWhere('nni', 'LIKE', "%$keyword%")
                ->orWhere('diplome', 'LIKE', "%$keyword%")
                ->orWhere('specialite', 'LIKE', "%$keyword%")
                ->orWhere('nationalite', 'LIKE', "%$keyword%")
                ->orWhere('genre', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $personnes = Personne::latest()->paginate($perPage);
        }

        return view('admin.personnes.index', compact('personnes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.personnes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'telephone' => 'required'
		]);
        $requestData = $request->all();
        
        Personne::create($requestData);

        return redirect('admin/personnes')->with('flash_message', 'Personne added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $personne = Personne::findOrFail($id);

        return view('admin.personnes.show', compact('personne'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $personne = Personne::findOrFail($id);

        return view('admin.personnes.edit', compact('personne'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'telephone' => 'required'
		]);
        $requestData = $request->all();
        
        $personne = Personne::findOrFail($id);
        $personne->update($requestData);

        return redirect('admin/personnes')->with('flash_message', 'Personne updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Personne::destroy($id);

        return redirect('admin/personnes')->with('flash_message', 'Personne deleted!');
    }
}
