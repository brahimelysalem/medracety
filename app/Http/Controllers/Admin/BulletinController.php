<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyClasseRequest;
use App\Http\Requests\StoreClasseRequest;
use App\Http\Requests\UpdateClasseRequest;
use App\Classe;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BulletinController extends Controller
{
    use CsvImportTrait;

    public function index()
    {
        //abort_if(Gate::denies('classes_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $classes = Classe::all();

        return view('admin.bulletins.index', compact('classes'));
    }

    public function create()
    {
        //abort_if(Gate::denies('classes_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bulletins.create');
    }

    public function store(StoreClasseRequest $request)
    {
        $classes = Classe::create($request->all());

        return redirect()->route('admin.bulletins.index');
    }

    public function edit(Classe $classes)
    {
        //abort_if(Gate::denies('classes_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bulletins.edit', compact('classes'));
    }

    public function update(UpdateClasseRequest $request, Classe $classes)
    {
        $classes->update($request->all());

        return redirect()->route('admin.bulletins.index');
    }

    public function show(Classe $classes)
    {
        //abort_if(Gate::denies('classes_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bulletins.show', compact('classes'));
    }

    public function destroy(Classe $classes)
    {
        //abort_if(Gate::denies('classes_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $classes->delete();

        return back();
    }

    public function massDestroy(MassDestroyClasseRequest $request)
    {
        Classe::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
