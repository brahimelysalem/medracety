<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotesTable extends Migration
{
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->float('valeur', 4, 2)->nullable();
            $table->timestamps();
            $table->unsignedInteger('eleve_id')->nullable();
            $table->foreign('eleve_id', 'eleve_fk_805713')->references('id')->on('eleves');
            $table->unsignedInteger('evaluation_id')->nullable();
            $table->foreign('evaluation_id', 'evaluation_fk_805714')->references('id')->on('evaluations');
            $table->softDeletes();
        });
    }
}
