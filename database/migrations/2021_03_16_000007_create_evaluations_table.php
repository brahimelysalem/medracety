<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluationsTable extends Migration
{
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_evaluation')->nullable();
            $table->string('type')->nullable();
            $table->unsignedInteger('matiere_id');
            $table->foreign('matiere_id', 'matiere_fk_805702')->references('id')->on('matieres');
            $table->unsignedInteger('classe_id');
            $table->foreign('classe_id', 'classe_fk_805702')->references('id')->on('classes');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
