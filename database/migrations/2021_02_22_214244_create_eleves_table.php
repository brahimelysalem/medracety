<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateElevesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eleves', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nni')->nullable();
            $table->string('prenom')->nullable();
            $table->string('prenom_ar')->nullable();
            $table->string('nom')->nullable();
            $table->string('nom_ar')->nullable();
            $table->date('date_naissance')->nullable();
            $table->string('lieu_naissance')->nullable();
            $table->string('lieu_naissance_ar')->nullable();
            $table->bigInteger('classe_id')->nullable();
            $table->date('date_inscription')->nullable();
            $table->float('mensualite')->nullable();
            $table->float('frais_inscription')->nullable();
            $table->boolean('exonere')->nullable();
            $table->date('date_sortie')->nullable();
            $table->string('telephone')->nullable();
            $table->string('telephone_2')->nullable();
            $table->string('matricule')->nullable();
            $table->float('moyenne_generale')->nullable();
            $table->string('decision')->nullable();
            $table->string('numero')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eleves');
    }
}
