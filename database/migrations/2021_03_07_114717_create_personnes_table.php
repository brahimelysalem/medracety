<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('telephone')->nullable();
            $table->string('telephone_2')->nullable();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->string('categorie')->nullable();
            $table->string('nni')->nullable();
            $table->string('diplome')->nullable();
            $table->string('specialite')->nullable();
            $table->string('nationalite')->nullable();
            $table->string('genre')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personnes');
    }
}
