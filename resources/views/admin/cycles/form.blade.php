<div class="form-group{{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', 'Libelle', ['class' => 'control-label']) !!}
    {!! Form::text('libelle', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
