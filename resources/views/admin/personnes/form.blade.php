<div class="form-group{{ $errors->has('telephone') ? 'has-error' : ''}}">
    {!! Form::label('telephone', 'Telephone', ['class' => 'control-label']) !!}
    {!! Form::text('telephone', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('telephone_2') ? 'has-error' : ''}}">
    {!! Form::label('telephone_2', 'Telephone 2', ['class' => 'control-label']) !!}
    {!! Form::text('telephone_2', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone_2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', 'Nom', ['class' => 'control-label']) !!}
    {!! Form::text('nom', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('prenom') ? 'has-error' : ''}}">
    {!! Form::label('prenom', 'Prenom', ['class' => 'control-label']) !!}
    {!! Form::text('prenom', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('categorie') ? 'has-error' : ''}}">
    {!! Form::label('categorie', 'Categorie', ['class' => 'control-label']) !!}
    {!! Form::text('categorie', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('categorie', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nni') ? 'has-error' : ''}}">
    {!! Form::label('nni', 'Nni', ['class' => 'control-label']) !!}
    {!! Form::text('nni', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nni', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('diplome') ? 'has-error' : ''}}">
    {!! Form::label('diplome', 'Diplome', ['class' => 'control-label']) !!}
    {!! Form::text('diplome', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('diplome', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('specialite') ? 'has-error' : ''}}">
    {!! Form::label('specialite', 'Specialite', ['class' => 'control-label']) !!}
    {!! Form::text('specialite', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('specialite', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nationalite') ? 'has-error' : ''}}">
    {!! Form::label('nationalite', 'Nationalite', ['class' => 'control-label']) !!}
    {!! Form::text('nationalite', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nationalite', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('genre') ? 'has-error' : ''}}">
    {!! Form::label('genre', 'Genre', ['class' => 'control-label']) !!}
    {!! Form::text('genre', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('genre', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
