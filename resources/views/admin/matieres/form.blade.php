<div class="form-group{{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', 'Libelle', ['class' => 'control-label']) !!}
    {!! Form::text('libelle', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('coeficient') ? 'has-error' : ''}}">
    {!! Form::label('coeficient', 'Coeficient', ['class' => 'control-label']) !!}
    {!! Form::number('coeficient', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('coeficient', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('base') ? 'has-error' : ''}}">
    {!! Form::label('base', 'Base', ['class' => 'control-label']) !!}
    {!! Form::number('base', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('base', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('niveau_id') ? 'has-error' : ''}}">
    {!! Form::label('niveau_id', 'Niveau scolaire', ['class' => 'control-label']) !!}
    {!! Form::select('niveau_id', $niveaux->pluck('libelle','id'),isset($matiere)? $matiere->niveau_id: '', ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('niveau_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('annee_id') ? 'has-error' : ''}}">
    {!! Form::label('annee_id', 'Année Scolaire', ['class' => 'control-label']) !!}
    {!! Form::select('annee_id', $annees->pluck('libelle','id'),isset($matiere)? $matiere->annee->libelle: '', ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('annee_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
