<div class="form-group{{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', 'Libelle', ['class' => 'control-label']) !!}
    {!! Form::text('libelle', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_debut') ? 'has-error' : ''}}">
    {!! Form::label('date_debut', 'Date Debut', ['class' => 'control-label']) !!}
    {!! Form::date('date_debut', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_debut', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_fin') ? 'has-error' : ''}}">
    {!! Form::label('date_fin', 'Date Fin', ['class' => 'control-label']) !!}
    {!! Form::date('date_fin', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_fin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_rupture') ? 'has-error' : ''}}">
    {!! Form::label('date_rupture', 'Date Rupture', ['class' => 'control-label']) !!}
    {!! Form::date('date_rupture', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_rupture', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
    {!! Form::text('type', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('montant') ? 'has-error' : ''}}">
    {!! Form::label('montant', 'Montant', ['class' => 'control-label']) !!}
    {!! Form::text('montant', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('montant', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('cycle') ? 'has-error' : ''}}">
    {!! Form::label('cycle', 'Cycle', ['class' => 'control-label']) !!}
    {!! Form::text('cycle', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('cycle', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('contact_id') ? 'has-error' : ''}}">
    {!! Form::label('contact_id', 'Contact Id', ['class' => 'control-label']) !!}
    {!! Form::text('contact_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('contact_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
