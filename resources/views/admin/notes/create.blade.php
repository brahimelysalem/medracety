@extends('layouts.backend')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('Ajouter') }} {{ trans('Note') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("notes.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="eleve_id">{{ trans('eleve') }}</label>
                <select class="form-control select2 {{ $errors->has('eleve') ? 'is-invalid' : '' }}" name="eleve_id" id="eleve_id">
                <option value="" > Selectionnez ...</option>
                    @foreach($eleves as  $eleve)
                        @if($eleve->classe != null)
                            <option value="{{ $eleve->id }}" > {{ $eleve->classe->niveau->libelle}} : {{ $eleve->prenom }} {{ $eleve->nom }}</option>
                        @endif
                    @endforeach
                </select>
                @if($errors->has('eleve_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('eleve_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('eleve_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="evaluation_id">{{ trans('evaluation') }}</label>
                <select class="form-control select2 {{ $errors->has('evaluation') ? 'is-invalid' : '' }}" name="evaluation_id" id="evaluation_id">
                <option value="" > Selectionnez ...</option>
                    @foreach($evaluations as  $evaluation)
                        @if($evaluation->matiere != null)
                            <option value="{{ $evaluation->id }}" > {{ $evaluation->type }} : {{ $evaluation->classe->niveau->libelle}} : {{ $evaluation->matiere->libelle}}  </option>
                        @endif
                    @endforeach
                </select>
                @if($errors->has('evaluation_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('evaluation_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('evaluation_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="valeur">{{ trans('valeur') }}</label>
                <input class="form-control {{ $errors->has('valeur') ? 'is-invalid' : '' }}" type="number" name="valeur" id="valeur" value="{{ old('valeur') }}" step="0.01" max="20">
                @if($errors->has('valeur'))
                    <div class="invalid-feedback">
                        {{ $errors->first('valeur') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('valeur_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('Enregistrer') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
