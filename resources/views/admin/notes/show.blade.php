@extends('layouts.backend')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('Voir') }} {{ trans('cruds.note.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('notes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('id') }}
                        </th>
                        <td>
                            {{ $note->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('eleve') }}
                        </th>
                        <td>
                            {{ $note->eleve->nom ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('evaluation') }}
                        </th>
                        <td>
                            {{ $note->evaluation->date_evaluation ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('valeur') }}
                        </th>
                        <td>
                            {{ $note->valeur }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('notes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
