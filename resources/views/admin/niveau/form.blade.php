<div class="form-group{{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', 'Libelle', ['class' => 'control-label']) !!}
    {!! Form::text('libelle', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('libelle_fr') ? 'has-error' : ''}}">
    {!! Form::label('libelle_fr', 'Libelle Fr', ['class' => 'control-label']) !!}
    {!! Form::text('libelle_fr', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('libelle_fr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('libelle_ar') ? 'has-error' : ''}}">
    {!! Form::label('libelle_ar', 'Libelle Ar', ['class' => 'control-label']) !!}
    {!! Form::text('libelle_ar', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('libelle_ar', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
