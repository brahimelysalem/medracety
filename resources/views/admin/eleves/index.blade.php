@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Eleves</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/eleves/create') }}" class="btn btn-success btn-sm" title="Add New Eleve">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        
                        <form method="POST" action="{{ route('eleves.excel.export') }}" >
                            {{ csrf_field() }}
                            <input type="text" name="name" placeholder="Nom de fichier" >
                            <select name="extension" >
                                <option value="xlsx" >.xlsx</option>
                                <option value="csv" >.csv</option>
                            </select>
                            <button type="submit" >Exporter</button>
                        </form>         
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/eleves', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Classe</th><th>Nni</th><th>Prenom</th><th>Prenom Ar</th><th>Telephone</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($eleves as $item)
                                    <tr>
                                        <td>{{ $item->numero }}</td>
                                        <td>{{ $item->classe->niveau->libelle }}</td><td>{{ $item->nni }}</td><td>{{ $item->prenom }}</td><td>{{ $item->prenom_ar }}</td><td>{{ $item->telephone }}</td>
                                        <td>
                                            <a href="{{ url('/admin/eleves/' . $item->id) }}" title="View Eleve"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/eleves/' . $item->id . '/edit') }}" title="Edit Eleve"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/eleves', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Eleve',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $eleves->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
