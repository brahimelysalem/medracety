<style type="text/css">
	body{
		font-family: 'dejavu sans';
		font-size: 12px;
	}
	table{
		border: 1px solid;
		border-collapse: collapse;
		margin: 0;
		width: 100% ;
	}
	td,th{
		padding: 3px 5px ;
		border: 1px solid;
		border-collapse: collapse;
	}

	tr{
		border-collapse: collapse;
	}
	.right{
		text-align: right;
	}
	.center{
		text-align: center;
	}
	h2,h3,h4,h5{margin: 0}

</style>


<div class="container " style="background-color: #ffffff; page-break-after: always;">
	<table style="border:0px" >
		<tr  style="border:0px">
			
			<td style="text-align: left;border:0px"><img style="height: 100px" src="{{ asset('/images/entete_fr.jpg') }}"></td>
			
			<td class="center" style="border:0px"><img style="height: 100px" src="{{  asset('/images/logo.jpg') }}"></td>
			
			<td class="right" style="border:0px"><img style="height: 100px" src="{{  asset('/images/entete_ar.jpg' )}}"></td>
			
		</tr>

		<tr  style="border:0px">
			<td colspan="3"  style="border:0px">
				<table>
					<tr>
						<td> Année Scolaire</td>
						<td> 2018 - 2019 </td>
						<td><h3 class="center">Bulletin  </h3></td>
						<td class="right" > 2019 - 2018 </td>
						<td class="right">السنة الدراسية</td>
					</tr>
					<tr>
						<td>Nom & Prénom</td>
						<td class="center"  colspan="3"> <h2>{{$student->nom}}</h2> </td>
						<td class="right">الإسم واللقب</td>
					</tr>
					<tr>
						<td>Classe</td>
						<td class="center"  colspan="3"> <h3>{{$student->room->name}} </h3></td>
						<td class="right">القسم</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="border:0px"><td colspan="3" style="border:0px">
				<table >   
					<tr>
						<th style="width: 220px">Matières</th>
						<th>1° Compo.</th>
						<th>2° Compo.</th>
						<th>3° Compo.</th>
						<th>Moy. Interrog.</th>
						<th>Moy. G.</th>
						<th>Coef.</th>
						<th>Total</th>
						<th>Observations</th>
					</tr>
					<?php
				   		$compo1=0;
				   		$compo2=0;
				   		$compo3=0;
				   		$dev=0;
				   		$total_mat=0;
				   		$mg_mat=0;
				   		$total_coef=0;
				   		$total=0;
				   		$mg=0;

				   	?>
				   @foreach($student->room->roomMatieres as $value)
				   		<?php
					   		$compo1=0;
					   		$compo2=0;
					   		$compo3=0;
					   		$dev=0;
					   	?>

					   @foreach($student->studentNotes as $grade)
					   		@if($grade->evaluation->matiere->id == $value->id)
							   <?php

							   		if($grade->evaluation->type == 'C1'){
							   			$compo1 = $grade->valeur;
							   		}
							   		elseif ($grade->evaluation->type == 'C2') {
							   			$compo2 =  $grade->valeur;
							   		}
							   		elseif ($grade->evaluation->type == 'C3') {
							   			$compo3 =  $grade->valeur;
							   		}
							   		elseif ($grade->evaluation->type == 'D') {
							   			$dev =  $grade->valeur;
							   		}
							   ?>
							@endif

					   @endforeach

					   <?php
					   		if($dev==0){
					   			$coefs=0;
					   			if($compo1>0)
					   				$coefs++;
					   			if($compo2>0)
					   				$coefs++;
					   			if($compo3>0)
					   				$coefs++;

					   			if(!$coefs>0)
					   				$coefs++;
					   			$dev=round(($compo1+$compo2+$compo3+1)/$coefs,2);
					   		}


					   		$total_mat = $compo1 + $compo2*2 + $compo3*3 + $dev*3;
					   		$mg_mat = round($total_mat/9,2);
					   		$total_mat =  $mg_mat*$value->coefficient;
					   		$total_coef += $value->coefficient;
					   		$total +=$total_mat;
					   	?>

				   <tr>
				   		<td celspacing="0">{{$value->libelle}}</td>
				   		<th>{{$compo1}}</th>
				   		<th>{{$compo2}}</th>
				   		<th>{{$compo3}}</th>
				   		<th>{{$dev}}</th>
				   		<th>{{$mg_mat}}</th>
				   		<th>{{$value->weight}}</th>
				   		<th>{{$total_mat}}</th>
				   </tr>
				   @endforeach

				   <tr>
				   		<td style="border:0px" colspan="4"></td>
				   		<th>Total</th>
				   		<th>{{$total}}</th>
				   		<th class="right" colspan="2">المجموع</th>
				   </tr>

				   <tr>
				   		<td style="border:0px" colspan="4"></td>
				   		<th>Coef.</th>
				   		<th>{{$total_coef}}</th>
				   		<th class="right" colspan="2">مجموع الضوارب</th>
				   </tr>

				   <tr>
				   		<td style="border:0px" colspan="4"></td>
				   		<th>M.G</th>
				   		<th></th>
				   		<th class="right" colspan="2">المعدل العام</th>
				   </tr>
				</table>
			</td>	
		</tr>
		<tr>
			
		</tr>


	</table>


	<br/>
	<br/>


	<table  style="width:100%;border:0">
		<tr>
			<td  style="width:40%;border:0">
				<table style="width:100%">
					<tr>
						<td style="width:45%"><h4>DECISION</h4></td>
						<td class="center"></td>
						<td style="width:45%" class="right"><h4>القرار</h4></td>
					</tr>
					<tr>
						<td style="width:45%"><h4>Passe en classe supérieure</h4></td>
						<td class="center"></td>
						<td style="width:45%" class="right"><h4>متجاوز إلى القسم الأعلى</h4></td>
					</tr>
					<tr>
						<td style="width:45%"><h4>Redoublement autorisé</h4></td>
						<td class="center"></td>
						<td style="width:45%" class="right"><h4>مرخص له في الإعادة</h4></td>
					</tr>
					<tr>
						<td style="width:45%"><h4>Exclusion définitive autorisé</h4></td>
						<td class="center"></td>
						<td style="width:45%" class="right"><h4>مطرود نهائيا</h4></td>
					</tr>
				</table>


			</td>
			<td  style="width:30%;border:0" class="center">
				<strong>Date : 05 Juin 2019 : التاريخ</strong>
				<br/>
				<br/>
				توقيع وختم المؤسسة
			
				<br/>
				Signature et cachet du directeur de l'Etablissement
			</td>
			<td  style="width:30%;border:0" class="right">
				<h3>Observations du directeur       ملاحظات المدير</h3>
				<p> ...................................................................................... ...................................................................................... ...................................................................................... ......................................................................................
				</p>

			</td>
		</tr>

	</table>


</div>