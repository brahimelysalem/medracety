<style type="text/css">
	body{
		font-family: 'dejavu sans';
		font-size: 12px;
	}
	table{
		border: 1px solid;
		border-collapse: collapse;
		margin: 0;
		width: 100% ;
	}
	td,th{
		padding: 3px 5px ;
		border: 1px solid;
		border-collapse: collapse;
	}

	tr{
		border-collapse: collapse;
	}
	.right{
		text-align: right;
	}
	.center{
		text-align: center;
	}
	h2,h3,h4,h5{margin: 0}

</style>


<div class="container " style="background-color: #ffffff; page-break-after: always;">
	<br/>
	<table style="border:0px" >
		<tr  style="border:0px">

			<td style="text-align: left;border:0px"><img style="height: 100px" src="{{ asset('/images/entete_fr.jpg') }}"></td>

			<td class="center" style="border:0px"><img style="height: 100px" src="{{  asset('/images/logo.jpg') }}"></td>

			<td class="right" style="border:0px"><img style="height: 100px" src="{{  asset('/images/entete_ar.jpg' )}}"></td>

		</tr>

		<tr  style="border:0px">
			<td style="border:0px"><br/></td>
		</tr>
		<tr  style="border:0px">
			<td colspan="3"  style="border:0px">
				<table>
					<tr>
						<td> Année Scolaire</td>
						<td> {{$eleve->classe->annee->libelle}} </td>
						<td><h3 class="center">Bulletin  </h3></td>
						<td class="right" > {{$eleve->classe->annee->libelle}}  </td>
						<td class="right">السنة الدراسية</td>
					</tr>
					<tr>
						<td>Nom & Prénom</td>
						<td class="center"  colspan="3"> <h2>{{$eleve->nom}}</h2> </td>
						<td class="right">الإسم واللقب</td>
					</tr>
					<tr>
						<td>Classe</td>
						<td class="center"  colspan="3"> <h3>{{$eleve->classe->niveau->libelle}} {{$eleve->classe->numero}} </h3></td>
						<td class="right">القسم</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr  style="border:0px">
			<td style="border:0px"><br/></td>
		</tr>
		<tr style="border:0px"><td colspan="3" style="border:0px">
				<table >
					<tr>
						<th style="width: 220px">Matières</th>
						<th>{{ $type_evaluation == 'C1' ? '1° Compo.' :  '' }} {{($type_evaluation == 'C2' ) ? '2° Compo.' :''}}</th>
						<th>Coef.</th>
						<th>Total</th>
						<th>Observations</th>
					</tr>
					<?php
				   		$compo1=0;
				   		$total_mat=0;
				   		$mg_mat=0;
				   		$total_coef=0;
				   		$total=0;
				   		$total_base=0;
				   		$mg=0;

				   	?>
                    <tr >
                        <td style="display: none" colspan="3">{{dump($eleve->notes)}}</td>
                    </tr>
                    <tr >
                        <td style="display: none" colspan="3">{{dump($eleve->classe->matieres())}}</td>
                    </tr>
				   @foreach($eleve->classe->matieres() as $value)
				   		<?php
					   		$compo1=0;
					   	?>

					    @foreach($eleve->notes as $grade)
					   		@if($grade->evaluation->matiere !=null && $grade->evaluation->matiere->id == $value->id)
							   <?php

							   		if($grade->evaluation->type == $type_evaluation){
							   			$compo1 = $grade->valeur;
							   		}
							   ?>
							@endif

					    @endforeach

					    <?php

					   		$total_mat = $compo1;
					   		$mg_mat = round($total_mat,2);
					   		$total_mat =  $mg_mat*$value->coeficient;
					   		$total_coef += $value->coeficient;
					   		$total += $total_mat;
					   		$total_base += $value->coeficient*$value->base;
					   	?>

				    <tr>
				   		<td celspacing="0">{{$value->libelle}}</td>
				   		<th>{{$compo1}} / {{$value->base}}</th>
				   		<th>{{$value->coeficient}}</th>
				   		<th>{{$total_mat}} / {{$value->coeficient * $value->base}}</th>
				    </tr>
				   @endforeach

				   <tr>
				   		<th></th>
				   		<th>Total</th>
				   		<th>{{$total_coef}}</th>
				   		<th>{{$total}} / {{$total_base}}</th>
				   		<th class="right" >المجموع</th>
				   </tr>

				   <tr>
				   		<th></th>
				   		<th>Coef.</th>
				   		<th></th>
				   		<th>{{$total_coef}}</th>
				   		<th class="right" >مجموع الضوارب</th>
				   </tr>

				   <tr>
				   		<th></th>
				   		<th>M.G</th>
				   		<th></th>
				   		<th>{{round($total * 20 / $total_base ,2)}}/20</th>
						   @php
						   if($eleve->classe->niveau->id < 8 && $type_evaluation ='C3')
						   		$eleve->setMG(round($total * 20 / $total_base ,2));
						   @endphp

				   		<th class="right">المعدل العام</th>
				   </tr>
				</table>
			</td>
		</tr>
		<tr>

		</tr>


	</table>


	<br/>
	<br/>


	<table  style="width:100%;border:0">
		<tr>

			<td  style="width:50%;border:0" class="center">
				<strong>Date : {{ \Carbon\Carbon::now()->locale('fr')->isoFormat('D MMMM YYYY') }} : التاريخ</strong>
				<br/>
				<br/>
				توقيع وختم المؤسسة

				<br/>
				Signature et cachet du directeur de l'Etablissement
			</td>
			<td  style="width:50%;border:0" class="right">
				<h3>Observations du directeur       ملاحظات المدير</h3>
				<p> ...................................................................................... ...................................................................................... ...................................................................................... ......................................................................................
				</p>

			</td>
		</tr>

	</table>


</div>
