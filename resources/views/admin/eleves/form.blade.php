<div class="form-group{{ $errors->has('nni') ? 'has-error' : ''}}">
    {!! Form::label('nni', 'Nni', ['class' => 'control-label']) !!}
    {!! Form::text('nni', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nni', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('prenom') ? 'has-error' : ''}}">
    {!! Form::label('prenom', 'Prenom', ['class' => 'control-label']) !!}
    {!! Form::text('prenom', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('prenom_ar') ? 'has-error' : ''}}">
    {!! Form::label('prenom_ar', 'Prenom Ar', ['class' => 'control-label']) !!}
    {!! Form::text('prenom_ar', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('prenom_ar', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', 'Nom', ['class' => 'control-label']) !!}
    {!! Form::text('nom', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nom_ar') ? 'has-error' : ''}}">
    {!! Form::label('nom_ar', 'Nom Ar', ['class' => 'control-label']) !!}
    {!! Form::text('nom_ar', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nom_ar', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_naissance') ? 'has-error' : ''}}">
    {!! Form::label('date_naissance', 'Date Naissance', ['class' => 'control-label']) !!}
    {!! Form::date('date_naissance', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_naissance', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('lieu_naissance') ? 'has-error' : ''}}">
    {!! Form::label('lieu_naissance', 'Lieu Naissance', ['class' => 'control-label']) !!}
    {!! Form::text('lieu_naissance', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('lieu_naissance', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('lieu_naissance_ar') ? 'has-error' : ''}}">
    {!! Form::label('lieu_naissance_ar', 'Lieu Naissance Ar', ['class' => 'control-label']) !!}
    {!! Form::text('lieu_naissance_ar', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('lieu_naissance_ar', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('classe_id') ? 'has-error' : ''}}">
    {!! Form::label('classe_id', 'Classe ', ['class' => 'control-label']) !!}
    {!! Form::select('classe_id', $classes->pluck('niveau.libelle','id'),isset($Eleve)?$Eleve->niveau_id : '', ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control'] ) !!}
    {!! $errors->first('classe_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_inscription') ? 'has-error' : ''}}">
    {!! Form::label('date_inscription', 'Date Inscription', ['class' => 'control-label']) !!}
    {!! Form::date('date_inscription', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_inscription', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('mensualite') ? 'has-error' : ''}}">
    {!! Form::label('mensualite', 'Mensualite', ['class' => 'control-label']) !!}
    {!! Form::number('mensualite', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('mensualite', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('frais_inscription') ? 'has-error' : ''}}">
    {!! Form::label('frais_inscription', 'Frais Inscription', ['class' => 'control-label']) !!}
    {!! Form::number('frais_inscription', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('frais_inscription', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('exonere') ? 'has-error' : ''}}">
    {!! Form::label('exonere', 'Exonere', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('%1$s', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('%1$s', '0', true) !!} No</label>
</div>
    {!! $errors->first('exonere', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_sortie') ? 'has-error' : ''}}">
    {!! Form::label('date_sortie', 'Date Sortie', ['class' => 'control-label']) !!}
    {!! Form::date('date_sortie', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_sortie', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('telephone') ? 'has-error' : ''}}">
    {!! Form::label('telephone', 'Telephone', ['class' => 'control-label']) !!}
    {!! Form::text('telephone', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('telephone_2') ? 'has-error' : ''}}">
    {!! Form::label('telephone_2', 'Telephone 2', ['class' => 'control-label']) !!}
    {!! Form::text('telephone_2', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone_2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('matricule') ? 'has-error' : ''}}">
    {!! Form::label('matricule', 'Matricule', ['class' => 'control-label']) !!}
    {!! Form::text('matricule', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('matricule', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('moyenne_generale') ? 'has-error' : ''}}">
    {!! Form::label('moyenne_generale', 'Moyenne Generale', ['class' => 'control-label']) !!}
    {!! Form::number('moyenne_generale', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('moyenne_generale', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('decision') ? 'has-error' : ''}}">
    {!! Form::label('decision', 'Decision', ['class' => 'control-label']) !!}
    {!! Form::text('decision', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('decision', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('numero') ? 'has-error' : ''}}">
    {!! Form::label('numero', 'Numero', ['class' => 'control-label']) !!}
    {!! Form::text('numero', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
