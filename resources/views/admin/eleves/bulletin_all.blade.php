<style type="text/css">
	body{
		font-family: 'dejavu sans';
		font-size: 12px;
	}
	table{
		border: 1px solid;
		border-collapse: collapse;
		margin: 0;
		width: 100% ;
	}
	td,th{
		padding: 3px 5px ;
		border: 1px solid;
		border-collapse: collapse;
	}

	tr{
		border-collapse: collapse;
	}
	.right{
		text-align: right;
	}
	.center{
		text-align: center;
	}
	h2,h3,h4,h5{margin: 0}

</style>


<div class="container " style="background-color: #ffffff; page-break-after: always;">
	<table style="border:0px" >
		<tr  style="border:0px">
			
			<td style="text-align: left;border:0px"><img style="height: 100px" src="{{ asset('/images/entete_fr.jpg') }}"></td>
			
			<td class="center" style="border:0px"><img style="height: 100px" src="{{  asset('/images/logo.jpg') }}"></td>
			
			<td class="right" style="border:0px"><img style="height: 100px" src="{{  asset('/images/entete_ar.jpg' )}}"></td>
			
		</tr>

		<tr  style="border:0px">
			<td style="border:0px"></td>
		</tr>
		<tr  style="border:0px">
			<td colspan="3"  style="border:0px">
				<table>
					<tr>
						<td> Année Scolaire</td>
						<td> {{$eleve->classe->annee->libelle}} </td>
						<td><h3 class="center">Bulletin  </h3></td>
						<td class="right" > {{$eleve->classe->annee->libelle}}  </td>
						<td class="right">السنة الدراسية</td>
					</tr>
					<tr>
						<td>Nom & Prénom</td>
						<td class="center"  colspan="3"> <h2>{{$eleve->prenom}} {{$eleve->nom}}  {{$eleve->prenom_ar}} {{$eleve->nom_ar}}</h2> </td>
						<td class="right">الإسم واللقب</td>
					</tr>
					<tr>
						<td>Classe</td>
						<td class="center"  colspan="3"> <h3>{{$eleve->classe->niveau->libelle}}{{$eleve->classe->numero}} </h3></td>
						<td class="right">القسم</td>
					</tr>
				</table>
			</td>
		</tr>	

		<tr  style="border:0px">
			<td style="border:0px"><br/></td>
		</tr>
		<tr style="border:0px"><td colspan="3" style="border:0px">
				<table >   
				<tr>
						<th style="width: 220px">Matières</th>
						<th>1° Compo.</th>
						<th>2° Compo.</th>
						<th>3° Compo.</th>
						<th>Moy. Interrog.</th>
						<th>Moy. G.</th>
						<th>Coef.</th>
						<th>Total</th>
						<th>Observations</th>
					</tr>
					<?php
				   		$compo1=0;
				   		$compo2=0;
				   		$compo3=0;
				   		$dev=0;
				   		$total_mat=0;
				   		$mg_mat=0;
				   		$total_coef=0;
				   		$total=0;
				   		$total_base=0;
				   		$mg=0;

				   	?>
				   @foreach($eleve->classe->matieres() as $value)
				   		<?php
					   		$compo1=0;
					   	?>

					   @foreach($eleve->notes as $grade)
							@if($grade->evaluation->matiere != null)
								@if($grade->evaluation->matiere->id == $value->id)
								<?php

										if($grade->evaluation->type == 'C1'){
											$compo1 = $grade->valeur;
										}
										if($grade->evaluation->type == 'C2'){
											$compo2 = $grade->valeur;
										}
										if($grade->evaluation->type == 'C3'){
											$compo3 = $grade->valeur;
										}
										if($grade->evaluation->type == 'D'){
											$dev = $grade->valeur;
										}
								?>
								@endif
							@endif
					   @endforeach

					   <?php

					   		$total_mat = ($compo1*1 + $compo2*2 + $compo3*3 +$dev*3)/9;
					   		$mg_mat = round($total_mat,2);
					   		$total_mat =  $mg_mat*$value->coeficient;
					   		$total_coef += $value->coeficient;
					   		$total += $total_mat;
					   		$total_base += $value->coeficient*$value->base;
					   	?>

				   <tr>
				   		<td celspacing="0">{{$value->libelle}}</td>
				   		<th>{{$compo1}} / {{$value->base}}</th>
				   		<th>{{$compo2}} / {{$value->base}}</th>
				   		<th>{{$compo3}} / {{$value->base}}</th>
				   		<th>{{$dev}} / {{$value->base}}</th>
				   		<th>{{$mg_mat}} / {{$value->base}}</th>
				   		<th>{{$value->coeficient}}</th>
				   		<th>{{$total_mat}} / {{$value->coeficient * $value->base}}</th>
				   </tr>
				   @endforeach

				   <tr>
				   		<th colspan="5"></th>
				   		<th>Total</th>
				   		<th>{{$total_coef}}</th>
				   		<th>{{$total}} / {{$total_base}}</th>
				   		<th class="right" >المجموع</th>
				   </tr>

				   <tr>
				   		<th colspan="5"></th>
				   		<th>Coef.</th>
				   		<th></th>
				   		<th>{{$total_coef}}</th>
				   		<th class="right" >مجموع الضوارب</th>
				   </tr>

				   <tr>
				   		<th colspan="5"></th>
				   		<th>M.G</th>
				   		<th></th>
				   		<th>{{round($total * 20 / $total_base ,2)}}/20</th>
						   @php
						   $eleve->setMG(round($total * 20 / $total_base ,2));
						   @endphp
				   		<th class="right">المعدل العام</th>
				   </tr>
				</table>
			</td>	
		</tr>

	</table>


	<br/>
	<br/>


	<table  style="width:100%;border:0">
		<tr>
			
			<td  style="width:50%;border:0" class="center">
				<strong>Date : 12 Juin  2023 : التاريخ</strong>
				<br/>
				<br/>
				توقيع وختم المؤسسة
			
				<br/>
				Signature et cachet du directeur de l'Etablissement
			</td>
			<td  style="width:50%;border:0" class="right">
				<h3>Observations du directeur       ملاحظات المدير</h3>
				<p> ...................................................................................... ...................................................................................... ...................................................................................... ......................................................................................
				</p>

			</td>
		</tr>

	</table>


</div>