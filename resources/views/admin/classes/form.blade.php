<div class="form-group{{ $errors->has('numero') ? 'has-error' : ''}}">
    {!! Form::label('numero', 'Numero', ['class' => 'control-label']) !!}
    {!! Form::number('numero', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('niveau_id') ? 'has-error' : ''}}">
    {!! Form::label('niveau_id', 'Niveau scolaire', ['class' => 'control-label']) !!}
    {!! Form::select('niveau_id', $niveaux->pluck('libelle','id'),isset($class )? $class->niveau_id: '', ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('niveau_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('annee_id') ? 'has-error' : ''}}">
    {!! Form::label('annee_id', 'Année Scolaire', ['class' => 'control-label']) !!}
    {!! Form::select('annee_id', $annees->pluck('libelle','id'),isset($class )? $class->annee->libelle_id: '', ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('annee_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
