@foreach($classe->eleves as $eleve )
	@if ($type_evaluation == 'All')
		@include('admin.eleves.bulletin_all',['eleve'=>$eleve , 'type_evaluation' => $type_evaluation ])
	@else
		@include('admin.eleves.bulletin',['eleve'=>$eleve , 'type_evaluation' => $type_evaluation ])
	@endif
@endforeach