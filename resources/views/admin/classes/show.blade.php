@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Classe {{ $class->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/bulletins/classe/' . $class->id.'/C1') }}" title="Compo1"><button class="btn btn-info btn-sm"><i class="fa fa-print" aria-hidden="true"></i> Compo1</button></a>
                        <a href="{{ url('/bulletins/classe/' . $class->id.'/C2') }}" title="Compo2"><button class="btn btn-info btn-sm"><i class="fa fa-print" aria-hidden="true"></i> Compo2</button></a>
                        <a href="{{ url('/bulletins/classe/' . $class->id.'/C3') }}" title="Compo3"><button class="btn btn-info btn-sm"><i class="fa fa-print" aria-hidden="true"></i> Compo3</button></a>
                        <a href="{{ url('/bulletins/classe/' . $class->id.'/All') }}" title="Compo3"><button class="btn btn-info btn-sm"><i class="fa fa-print" aria-hidden="true"></i> Grand bulletin</button></a>
                        <a href="{{ route('listes.classe.eleves',['classe_id'=>$class->id]) }}" title="Imprimer"><button class="btn btn-outline-dark btn-sm"><i class="fa fa-print" aria-hidden="true"></i> Imprimer</button></a>
                        <a href="{{ url('/admin/classes') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/classes/' . $class->id . '/edit') }}" title="Edit Class"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/classes', $class->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Class',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $class->id }}</td>
                                    </tr>
                                    <tr><th> Numero </th><td> {{ $class->numero }} </td></tr><tr><th> Niveau Id </th><td> {{ $class->niveau->libelle }} </td></tr><tr><th> Annee Id </th><td> {{ $class->annee->libelle }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Classe</th><th>Nni</th><th>Prenom</th><th>Prenom Ar</th><th>Telephone</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($class->eleves as $item)
                                    <tr>
                                        <td>{{ $item->numero }}</td>
                                        <td>{{ $item->classe->niveau->libelle }}</td><td>{{ $item->nni }}</td><td>{{ $item->prenom }}</td><td>{{ $item->prenom_ar }}</td><td>{{ $item->telephone }}</td>
                                        <td>'bulletins/classe/{classe}/{eval}'
                                           <a href="{{ url('/admin/eleves/' . $item->id) }}" title="View Eleve"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/eleves/' . $item->id . '/edit') }}" title="Edit Eleve"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/eleves', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Eleve',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
