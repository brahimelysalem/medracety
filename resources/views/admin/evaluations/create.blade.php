@extends('layouts.backend')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('Ajouter') }} {{ trans('Evaluation') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <table class=" table table-bordered table-striped table-hover">
                <tr data-entry-id="{{ $classe->id }}">
                    <td>
                        Classe :
                    </td>
                    <td>
                        {{ $classe->nom ?? '' }}
                    </td>
                    <td>
                        {{ $classe->annee->libelle ?? '' }}
                    </td>
                    <td>
                        {{ App\Models\Classe::ETAT_RADIO[$classe->etat] ?? '' }}
                    </td>

                </tr>
            </table>
        </div>
        <hr/>
        <form method="POST" action="{{ route("evaluations.store") }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="classe_id" value="{{$classe->id}}" />
            <div class="form-group">
                <label for="date_evaluation">{{ trans('date_evaluation') }}</label>

                <input class="form-control date {{ $errors->has('date_evaluation') ? 'is-invalid' : '' }}" type="text" name="date_evaluation" id="date_evaluation" value="{{ old('date_evaluation') }}">
                @if($errors->has('date_evaluation'))
                    <div class="invalid-feedback">
                        {{ $errors->first('date_evaluation') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('date_evaluation_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="matiere_id">{{ trans('matiere') }}</label>
                <select class="form-control select2 {{ $errors->has('matiere') ? 'is-invalid' : '' }}" name="matiere_id" id="matiere_id" required>
                    @foreach($matieres as $id => $matiere)
                        <option value="{{ $id }}" {{ old('matiere_id') == $id ? 'selected' : '' }}>{{ $matiere }}</option>
                    @endforeach
                </select>
                @if($errors->has('matiere_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('matiere_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('matiere_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type">
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('Slectionner ...') }}</option>
                    @foreach(App\Models\Evaluation::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('type_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('Enregistrer') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
