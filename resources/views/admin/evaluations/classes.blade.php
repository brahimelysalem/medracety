@extends('layouts.backend')
@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('Classe') }} {{ trans('Liste') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Classe">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                           #
                        </th>
                        <th>
                            Classe
                        <th>
                            Année
                        </th>
                        <th>
                            Etat
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($classes as $key => $classes)
                        <tr data-entry-id="{{ $classes->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $classes->id ?? '' }}
                            </td>
                            <td>
                                {{ $classes->niveau->libelle ?? '' }}
                            </td>
                            <td>
                                {{ $classes->annee->libelle ?? '' }}
                            </td>
                            <td>
                                {{ App\Models\Classe::ETAT_RADIO[$classes->etat] ?? '' }}
                            </td>
                            <td>
                                <a class="btn btn-success" href="{{ url('/admin/evaluations/create/'.$classes->id) }}">
                                    {{ trans('Ajouter') }} {{ trans('Evaluation') }}
                                </a>

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('classes_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.classes.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Classe:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection
