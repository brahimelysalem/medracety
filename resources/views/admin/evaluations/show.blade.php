@extends('layouts.backend')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('Voir') }} {{ trans('cruds.evaluation.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('evaluations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
                @foreach(App\Models\Evaluation::TYPE_SELECT as $key => $label)
                    <option value="{{ $key }}" {{ old('type', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    <a class="btn btn-primary" href="{{ route('evaluations.index') }}">
                        {{ $label }}
                    </a>
                @endforeach
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr data-entry-id="{{ $evaluation->id }}">
                        <td>

                        </td>
                        <td>
                            {{ $evaluation->classe->niveau->libelle ?? '' }}
                        </td>
                        <td>
                            {{ $evaluation->date_evaluation ?? '' }}
                        </td>
                        <td>
                            {{ $evaluation->matiere->libelle ?? '' }}
                        </td>
                        <td>
                            {{ App\Models\Evaluation::TYPE_SELECT[$evaluation->type] ?? '' }}
                        </td>

                    </tr>
                </tbody>
            </table>

            <table class=" table table-bordered table-striped table-hover datatable datatable-Note">
                <thead>
                    <tr>
                        <th>
                            {{ trans('Eleve') }}
                        </th>
                        <th>
                            {{ trans('Note') }} / {{ $evaluation->matiere->base ?? '' }}
                        </th>

                        <th>
                            {{ trans('الإسم') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($evaluation->evaluationNotes as $key => $note)
                        <tr data-entry-id="{{ $note->id }}">
                            <td>
                                {{ $note->eleve->prenom ?? '' }} {{ $note->eleve->nom ?? '' }}
                            </td>
                            <td>
                                {{ $note->valeur ?? '' }}
                            </td>
                            <td>
                                {{ $note->eleve->prenom_ar ?? '' }} {{ $note->eleve->nom_ar ?? '' }}
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="form-group">
                <a class="btn btn-default" href="{{ route('evaluations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
