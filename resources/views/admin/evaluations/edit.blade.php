@extends('layouts.backend')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('Modifier') }} {{ trans('Evaluation') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("evaluations.update", [$evaluation->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="date_evaluation">{{ trans('date_evaluation') }}</label>
                <input class="form-control date {{ $errors->has('date_evaluation') ? 'is-invalid' : '' }}" type="text" name="date_evaluation" id="date_evaluation" value="{{ old('date_evaluation', $evaluation->date_evaluation) }}">
                @if($errors->has('date_evaluation'))
                    <div class="invalid-feedback">
                        {{ $errors->first('date_evaluation') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('date_evaluation_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="matiere_id">{{ trans('matiere') }}</label>
                <select class="form-control select2 {{ $errors->has('matiere') ? 'is-invalid' : '' }}" name="matiere_id" id="matiere_id" required>
                    @foreach($matieres as $id => $matiere)
                        <option value="{{ $id }}" {{ ($evaluation->matiere ? $evaluation->matiere->id : old('matiere_id')) == $id ? 'selected' : '' }}>{{ $matiere }}</option>
                    @endforeach
                </select>
                @if($errors->has('matiere_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('matiere_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('matiere_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type">
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('Slectionner ...') }}</option>
                    @foreach(App\Models\Evaluation::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', $evaluation->type) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('type_helper') }}</span>
            </div>


             <table class=" table table-bordered table-striped table-hover datatable datatable-Note">
                <thead>
                    <tr>
                        <th>
                            {{ trans('eleve') }}
                        </th>
                        <th>
                            {{ trans('valeur') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($evaluation->evaluationNotes as $key => $note)
                        <tr data-entry-id="{{ $note->id }}" style="border:solid 1px black">
                            <td>
                                {{ $note->eleve->prenom ?? '' }} {{ $note->eleve->nom ?? '' }}
                            </td>
                            <td>
                                <label style="width:40px">{{ $note->valeur ?? '' }}</label>
                                <input  name="notes[{{$note->id}}]" id="notes[{{$note->id}}]" value="{{ $note->valeur ?? '0' }}">
                            </td>
                            <td>
                                {{ $note->eleve->prenom_ar ?? '' }} {{ $note->eleve->nom_ar ?? '' }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('Enregistrer') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
