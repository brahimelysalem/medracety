@extends('layouts.backend')
@section('content')
<div class="container">

    <a class="btn btn-success" href="{{ route("evaluations.create") }}">
        Nouvelle évaluation
    </a>
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    Liste des évaluations
                </div>
                <div class="card-body">
                    <form method="get" class="inline">
                        <select name="classe_id" id="classe_id" >
                        <option value="">Tous</option>
                            @foreach($classes as $classe)
                                <option value="{{$classe->id}}" {{ ($classe_id == $classe->id)? 'selected' : '' }}>{{ $classe->niveau->libelle ?? '' }}</option>
                            @endforeach
                        </select>
                        <select name="type" id="type" >
                            <option value="">Tous</option>
                            <option value="D" {{ ($type == 'D')? 'selected' : '' }}>Devoir</option>
                            <option value="C1" {{ ($type == 'C1')? 'selected' : '' }}>Composition 1</option>
                            <option value="C2" {{ ($type == 'C2')? 'selected' : '' }}>Composition 2</option>
                            <option value="C3" {{ ($type == 'C3')? 'selected' : '' }}>Composition 3</option>
                        </select>
                        <button type="submit">Recherche</button>
                    </form>
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-Evaluation">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        Classe
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Matière
                                    </th>
                                    <th>
                                        /Base
                                    </th>
                                    <th>
                                        Coef.
                                    </th>
                                    <th>
                                        Type
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($evaluations as $key => $evaluation)
                                    <tr data-entry-id="{{ $evaluation->id }}">
                                        <td>

                                        </td>
                                        <td>
                                            {{ $evaluation->classe->niveau->libelle ?? ''  }} {{ $evaluation->classe->numero ?? ''  }}
                                        </td>
                                        <td>
                                            {{ $evaluation->date_evaluation ?? '' }}
                                        </td>
                                        <td>
                                            {{ $evaluation->matiere->libelle ?? '' }}
                                        </td>
                                        <td>
                                            {{ $evaluation->matiere->base ?? '' }}
                                        </td>
                                        <td>
                                            {{ $evaluation->matiere->coefficient ?? '' }}
                                        </td>
                                        <td>
                                            {{ App\Models\Evaluation::TYPE_SELECT[$evaluation->type] ?? '' }}
                                        </td>
                                        <td>
                                            <a class="btn btn-xs btn-primary" href="{{ route('evaluations.show', $evaluation->id) }}">
                                                {{ trans('Afficher') }}
                                            </a>
                                            <a class="btn btn-xs btn-info" href="{{ route('evaluations.edit', $evaluation->id) }}">
                                                {{ trans('Modifier') }}
                                            </a><form action="{{ route('evaluations.destroy', $evaluation->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('Supprimer') }}">
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('evaluation_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('evaluations.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Evaluation:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection
