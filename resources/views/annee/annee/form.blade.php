<div class="form-group{{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', 'Libelle', ['class' => 'control-label']) !!}
    {!! Form::text('libelle', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_debut') ? 'has-error' : ''}}">
    {!! Form::label('date_debut', 'Date Debut', ['class' => 'control-label']) !!}
    {!! Form::date('date_debut', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_debut', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_fin') ? 'has-error' : ''}}">
    {!! Form::label('date_fin', 'Date Fin', ['class' => 'control-label']) !!}
    {!! Form::date('date_fin', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_fin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('ouverte') ? 'has-error' : ''}}">
    {!! Form::label('ouverte', 'Ouverte', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('ouverte', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('ouverte', '0', true) !!} No</label>
</div>
    {!! $errors->first('ouverte', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('precedente_id') ? 'has-error' : ''}}">
    {!! Form::label('precedente_id', 'Precedente Id', ['class' => 'control-label']) !!}
    {!! Form::number('precedente_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('precedente_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
