<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>


        <!-- Styles -->
        <style>
            table,thead,tbody,tr
            {
                width: 100%;
            }
            table,
            td , th{
                border: 1px solid #333;
                height: 25px;
            }

            thead,
            tfoot {
                background-color: #333;
                color: #fff;
            }
            .page-break {
                page-break-after: always;
            }
        </style>
    </head>
<body>
    <h1 >Liste des élèves de la classe : {{$classe->niveau->libelle.$classe->numero.'- '.$classe->annee->libelle}}</h1>
    <table>
        <thead>
            <tr>
                <th style="width: 170px">Nom</th>
                <th style="width: 90px">NNI</th>
                <th style="width: 70px">Téléphone</th>
                <th style="text-align: right; width: 170px">Nom en ARABE </th>
                <th style="width: 25px">N°</th>
            </tr>
        </thead>
        <tbody>
            @foreach($classe->eleves as $item)
                <tr>
                    <td style="width: 170px">{{ $item->prenom }} {{ $item->nom }}</td>
                    <td style="width: 90px">{{ $item->nni }}</td>
                    <td style="width: 70px">{{ $item->telephone }}</td>
                    <td style="text-align: right; width: 170px">{{ $item->prenom_ar }} {{ $item->nom_ar }}</td>
                    <td style="width: 25px">{{ $item->numero }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
