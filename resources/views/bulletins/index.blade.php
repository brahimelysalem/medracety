@extends('layouts.backend')
@section('content')
@can('classes_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("classes.create") }}">
                {{ trans('Ajouter') }} {{ trans('Classe') }}
            </a>
            <button class="btn btn-warning" data-toggle="modal" data-target="#csvImportModal">
                {{ trans('global.app_csvImport') }}
            </button>
            @include('csvImport.modal', ['model' => 'classes', 'route' => 'admin.classes.parseCsvImport'])
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('Classe') }} {{ trans('Liste') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Classe">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.classe.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.classe.fields.nom') }}
                        </th>
                        <th>
                            {{ trans('cruds.classe.fields.annee') }}
                        </th>
                        <th>
                            {{ trans('cruds.classe.fields.etat') }}
                        </th>
                        <th>
                            {{ App\Models\Evaluation::TYPE_SELECT['C1'] }}
                        </th>
                        <th>
                            {{ App\Models\Evaluation::TYPE_SELECT['C2'] }}
                        </th>
                        <th>
                            {{ App\Models\Evaluation::TYPE_SELECT['C3'] }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($classes as $key => $classes)
                        <tr data-entry-id="{{ $classes->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $classes->id ?? '' }}
                            </td>
                            <td>
                                {{ $classes->nom ?? '' }}
                            </td>
                            <td>
                                {{ $classes->annee->libelle ?? '' }}
                            </td>
                            <td>
                                {{ App\Models\Classe::ETAT_RADIO[$classes->etat] ?? '' }}
                            </td>
                            <th>
                                {{ App\Models\Evaluation::TYPE_SELECT['C1'] }}
                            </th>
                            <th>
                                {{ App\Models\Evaluation::TYPE_SELECT['C2'] }}
                            </th>
                            <th>
                                {{ App\Models\Evaluation::TYPE_SELECT['C3'] }}
                            </th>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


      $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'desc' ]],
        pageLength: 100,
      });
      $('.datatable-Classe:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })

</script>
@endsection
