@extends('layouts.backend')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('Voir') }} {{ trans('cruds.classe.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.classes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.classe.fields.id') }}
                        </th>
                        <td>
                            {{ $classes->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.classe.fields.nom') }}
                        </th>
                        <td>
                            {{ $classes->nom }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.classe.fields.niveau') }}
                        </th>
                        <td>
                            {{ $classes->niveau }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.classe.fields.annee') }}
                        </th>
                        <td>
                            {{ $classes->annee->libelle }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.classe.fields.etat') }}
                        </th>
                        <td>
                            {{ App\Models\Classe::ETAT_RADIO[$classes->etat] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.classe.fields.base') }}
                        </th>
                        <td>
                            {{ $classes->base }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.classes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
