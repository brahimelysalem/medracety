@extends('layouts.backend')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('Modifier') }} {{ trans('Classe') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("classes.update", [$classes->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="nom">{{ trans('cruds.classe.fields.nom') }}</label>
                <input class="form-control {{ $errors->has('nom') ? 'is-invalid' : '' }}" type="text" name="nom" id="nom" value="{{ old('nom', $classes->nom) }}">
                @if($errors->has('nom'))
                    <div class="invalid-feedback">
                        {{ $errors->first('nom') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.classe.fields.nom_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="niveau">{{ trans('cruds.classe.fields.niveau') }}</label>
                <input class="form-control {{ $errors->has('niveau') ? 'is-invalid' : '' }}" type="text" name="niveau" id="niveau" value="{{ old('niveau', $classes->niveau) }}">
                @if($errors->has('niveau'))
                    <div class="invalid-feedback">
                        {{ $errors->first('niveau') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.classe.fields.niveau_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="annee">{{ trans('cruds.classe.fields.annee') }}</label>
                <input class="form-control {{ $errors->has('annee') ? 'is-invalid' : '' }}" type="text" name="annee" id="annee" value="{{ old('annee', $classes->annee->libelle) }}">
                @if($errors->has('annee'))
                    <div class="invalid-feedback">
                        {{ $errors->first('annee') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.classe.fields.annee_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.classe.fields.etat') }}</label>
                @foreach(App\Models\Classe::ETAT_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('etat') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="etat_{{ $key }}" name="etat" value="{{ $key }}" {{ old('etat', $classes->etat) === (string) $key ? 'checked' : '' }}>
                        <label class="form-check-label" for="etat_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('etat'))
                    <div class="invalid-feedback">
                        {{ $errors->first('etat') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.classe.fields.etat_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="base">{{ trans('cruds.classe.fields.base') }}</label>
                <input class="form-control {{ $errors->has('base') ? 'is-invalid' : '' }}" type="number" name="base" id="base" value="{{ old('base', $classes->base) }}" step="1">
                @if($errors->has('base'))
                    <div class="invalid-feedback">
                        {{ $errors->first('base') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.classe.fields.base_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('Enregistrer') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
